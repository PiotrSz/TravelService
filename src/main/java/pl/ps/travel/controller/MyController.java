package pl.ps.travel.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
public class MyController {
    @RequestMapping("/")
    public String indexPage() {
        return "index";
    }

    @RequestMapping("/myAccount")
    public String myAccountPage(){
        return "myAccount";
    }

    @RequestMapping("/register")
    public String registerPage(){
        return "register";
    }
}
